# Introdução
A API para consultar  pedidos


### Solicitação GET
> /Carriers/Tracking/{PedidoCliente}

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
PedidoCliente|Chave identificadora do pedido|sim|String
 


### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "status": "erro",
    "errors": {
        "servidor": [
            "Erro no servidor"
        ]
    }
}
```

Erros de requisição (erro 400)
```JS
{
    "status": "erro",
    "errors": {
        "Tracking": [
            "dados de Tracking não encontrado"
        ]
    }
}
```
# Lista de Status
|idStatus|Status|Descrição             
|----------------|---------------|---------------|
22|	COLETADO|	Recebido no CD da transportadora
24|	CANCELADO|	Pedido não recebido pela transportadora
31|	SEPARADO|	Em preparação para transporte
50|	EM TRANSFERÊNCIA PARA A BASE|	Em transferência para filial Distribuidora
51|	RECEBIDO NA BASE|	Na filial distribuidora
100|	EM ROTA DE ENTREGA|	Em rota de entrega
101|	ENTREGA REALIZADA|	Entregue
102|	INSUCESSO DE ENTREGA|	Com insucesso de entrega
150|	EM DROPPOINT|	Em Drop Point
151|	EDROP REALIZADO|	Entregue
200|	PENDENCIA DE ENTREGA|	Na filial distribuidora
411|	DEVOLUÇÃO REALIZADA|	Pedido devolvido à loja



## Resposta

```JS
{
    "PedidoCliente": "0012001029279",
    "idItemParceiro": 12072422,
    "Cliente": "MUNDO INFANTIL",
    "Destinatario": "JAQUELINE SIMAO PEREIRA VIEIRA",
    "codigoRastreio": "145-0012001029279-14807165",
    "Url": "https://www.carriers.com.br/portal/localizador.php?l=145-0012001029279-14807165",
    "ValorFrete": "17.68",
    "Eventos": [
        {
            "Data": "27-01-2020 09:51:39",
            "Status": "ENTREGA REALIZADA",
            "idStatus": 101,
            "Descricao": "Entregue"
        },
        {
            "Data": "27-01-2020 08:57:16",
            "Status": "EM ROTA DE ENTREGA",
            "idStatus": 100,
            "Descricao": "Em rota de entrega"
        },
        {
            "Data": "27-01-2020 07:26:29",
            "Status": "RECEBIDO NA BASE",
            "idStatus": 51,
            "Descricao": "Na filial distribuidora"
        },
        {
            "Data": "26-01-2020 22:35:30",
            "Status": "EM TRANSFERÊNCIA PARA A BASE",
            "idStatus": 50,
            "Descricao": "Em transferência para filial Distribuidora"
        },
        {
            "Data": "24-01-2020 22:53:41",
            "Status": "COLETADO",
            "idStatus": 22,
            "Descricao": "Recebido no CD da transportadora"
        },
        {
            "Data": "24-01-2020 22:53:41",
            "Status": "ETIQUETADO SEPARAÇÃO",
            "idStatus": 31,
            "Descricao": "Em preparação para transporte"
        }
    ]
}
```
