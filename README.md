[![N|Solid](https://www.carriers.com.br/site/img/logos/logo.png)](https://nodesource.com/products/nsolid)
# API CARRIERS
____
API para cadastros e consultas de pedidos, verficações de trackings e fretes.

# Requisição
> Homologacao: http://35.247.213.54/api/client/

> Producao: http://api.carriers.com.br/client/

```JSON
 curl -X GET \
   http://35.247.213.54/api/client/ \
  -H 'Authorization: Bearer <API-KEY>' \
  -H 'cache-control: no-cache'
  ```

* Cadastro de pedidos:
    * [Cadastro de pedidos](ConsultaPedidos/CadastroPedidos.md)

* Consulta Tracking:
    * [Consulta de Tracking](ConsultaPedidos/ConsultaTracking.md)
    * [Consulta de Tracking V2](ConsultaPedidos/ConsultaTrackingV2.md)

* Consulta Frete:
    * [Consulta de Frete](ConsultaPedidos/ConsultaFrete.md)
    * [Consulta de Frete por volume](ConsultaPedidos/ConsultaFreteVolume.md)